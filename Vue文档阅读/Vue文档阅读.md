
Vue 的整体思想是拥抱经典的 Web 技术，并在其上进行扩展是一套用于构建用户界面的渐进式框架。vue2 最低只能支持到 IE9。


# \# 对比其他框架
React 和 Vue 有许多相似之处，它们都有：
  - 使用 Virtual DOM
  - 提供了响应式 (Reactive) 和组件化 (Composable) 的视图组件。
  - 将注意力集中保持在核心库，而将其他功能如路由和全局状态管理交给相关的库。

Vue
  - 也提供了渲染函数，甚至支持 JSX。然而，我们默认推荐的还是模板。
  - Vue 设置样式的默认方法是单文件组件。(react是 [CSS-in-JS][1])
  - 起步阶段不需学 JSX，ES2015 以及构建系统, 上手快

Angular
  - 事实上必须用 TypeScript 来开发，因为它的文档和学习资源几乎全部是面向 TS 的。TS 有很多好处——静态类型检查在大规模的应用中非常有用，同时对于 Java 和 C# 背景的开发者也是非常提升开发效率的。

# \# 小知识/小技巧
- Vue 实例还暴露了一些有用的实例属性与方法。它们都有前缀 $，以便与用户定义的属性区分开来。
```
vm.$data === data // => true
vm.$el === document.getElementById('example') // => true

// $watch 是一个实例方法
vm.$watch('a', function (newValue, oldValue) {
  // 这个回调将在 `vm.a` 改变后调用
})
```
- v-once 能执行一次性地插值，当数据改变时，插值处的内容不会更新。之前是 * , 现在不行


# \# 实例生命周期钩子
8个钩子
- 构建前/后 beforeCreate/create
- 载入前/后 beforeMount/mounted
- 更新前/后 beforeUpdate/update
- 销毁前/后 beforeDestroy/destroyed

# \# 组件化
- 在 Vue 中注册组件很简单：
```
Vue.component('todo-item', {
  template : '<li>这是一个待办事项</li>'
})
```
- 在 Vue 中使用组件
```
<ol>
  <!-- 创建一个 todo-item 组件的实例 -->
  <todo-item></todo-item>
</ol>
```
- 子组件接受父组件传递的数据
```
Vue.component('todo-item', {
  // todo-item 组件现在接受一个 "prop"，类似于一个自定义特性。这个 prop 名为 todo。
  props : ['todo'] ,
  template : '<li>{{ todo.text }}</li>'
})

<todo-item
  v-for="item in groceryList"
  v-bind:todo="item"
  v-bind:key="item.id">
</todo-item>
```
- Vue 的模板语法, 支持使用 JavaScript 表达式, 但不支持语句与留控制 :
  - 语句指的是 : var a = 1 等
  - 流控制指的是 : if(){} , while(){}, for(){} 等

- 指令 (Directives) 是带有 v- 前缀的特殊特性。
  常见的指令: (Vue.js 为 v-bind 和 v-on 这两个最常用的指令，提供了特定简写)
  v-bind  (:)
  v-on    (@)
  v-if
  v-for

- 修饰符 (Modifiers) 是以半角句号 . 指明的特殊后缀 

- 计算属性 (computed)
  顾名思义: 计算后的属性, 可以使用在 :class 这样的, 可以使用在 v-model 这样的,  
  ```
  computed : {
    // 计算属性的 getter
    reversedMessage: function () {
      // `this` 指向 vm 实例
      return this.message.split('').reverse().join('')
    }
  }
  ```

- 侦听属性 (watch)
  观察和响应 Vue 实例上的数据变动, 并影响到 data 上的数据
  ```
  watch: {
    firstName: function (val) {
      this.fullName = val + ' ' + this.lastName
    },
    lastName: function (val) {
      this.fullName = this.firstName + ' ' + val
    }
  }
  ```

# \# Class 与 Style 绑定
- 对象语法
  `<div v-bind:style="{ color: activeColor, fontSize: fontSize + 'px' }"></div>`
  
- 多重值, 从 2.3.0 起你可以为 style 绑定中的属性提供一个包含多个值的数组，常用于提供多个带前缀的值，例如：
  `<div :style="{ display: ['-webkit-box', '-ms-flexbox', 'flex'] }"></div>`

# \# 条件渲染 v-if
- 当 v-if 与 v-for 一起使用时，v-for 具有比 v-if 更高的优先级。这意味着 v-if 将分别重复运行于每个 v-for 循环中。
- `<template>` 元素上使用 v-if 条件渲染分组, 最终的渲染结果将不包含 `<template>` 元素。
```
  <template v-if="ok">
    <h1 v-for='i in arr'>Title</h1>
  </template>
```
- Vue中的数组
  - 当你利用索引直接设置一个项时，例如：vm.items[indexOfItem] = newValue
    - 可以 `Vue.set(vm.items, indexOfItem, newValue)`
    - 可以 `vm.items.splice(indexOfItem, 1, newValue)`
  - 当你修改数组的长度时，例如：vm.items.length = newLength
    - 可以 `vm.items.splice(newLength)`
- Vue中的对象
  - 可以使用 `Vue.set(object, key, value)` 方法向嵌套对象添加响应式属性
    - 例: `Vue.set(vm.userProfile, 'age', 27)`
  - 可以使用以下实习添加一个N多属性
    ```
    vm.userProfile = Object.assign({}, vm.userProfile, {
      age: 27,
      favoriteColor: 'Vue Green'
    })
    ```

- 显示过滤/排序结果
  ```js
  <li v-for="n in evenNumbers">{{ n }}</li>
  data: {
    numbers: [ 1, 2, 3, 4, 5 ]
  },
  computed: {
    evenNumbers: function () {
      return this.numbers.filter(function (number) {
        return number % 2 === 0
      })
    }
  }
  ```


# \# 列表渲染 v-for
`<li v-for="(item, index) in Arrs"></li>`
或者
`<li v-for="(item, index) of Arrs"></li>`
或者
`<li v-for="(value, key, index) of object"></li>` (在遍历对象时，是按 Object.keys() 的结果遍历，但是不能保证它的结果在不同的 JavaScript 引擎下是一致的。即: 排序未必是对的)


# \# 事件处理
- 可以用特殊变量 $event 作为参数访问原始的 DOM 事件
- 也可以没有参数, 但是在事件内使用 event 访问原始的 DOM 事件
- *事件修饰符* (!使用修饰符时，顺序很重要；)
  .stop // 阻止冒泡
  .prevent // 阻止默认
  .capture // 使用事件捕获模式, 即元素自身触发的事件先在此处理，然后才交由内部元素进行处理
  .self // 只当在 event.target 是当前元素自身时触发处理函数, 即事件不是从内部元素触发的
  .once // 点击事件将只会触发一次, 2.1.4 新增
  .passive // 尤其能够提升移动端的性能, 使浏览器默认行为将会立即触发事件
- 不要把 .passive 和 .prevent 一起使用，因为 .prevent 将会被忽略，同时浏览器可能会向你展示一个警告。请记住，.passive 会告诉浏览器你不想阻止事件的默认行为。
- *按键修饰符* : 
  - 全部的按键别名：
  .enter/.13
  .tab
  .delete (捕获“删除”和“退格”键)
  .esc
  .space
  .up
  .down
  .left
  .right    
  - 通过全局 config.keyCodes 对象自定义按键修饰符别名：
  ```js
  // 可以使用 `v-on:keyup.f1`
  Vue.config.keyCodes.f1 = 112
  Vue.config.keyCodes.enter = 13
  ```
- *系统修饰键*
  .ctrl  // 2.1.0 新增
  .alt   // 2.1.0 新增
  .shift // 2.1.0 新增
  .meta  // 2.1.0 新增
  .exact // 2.5.0 新增
  ```html
  <!-- Alt + C -->
  <input @keyup.alt.67="clear">

  <!-- Ctrl + Click -->
  <div @click.ctrl="doSomething">Do something</div>
  ```

- 为什么在 HTML 中监听事件?
  - 扫一眼 HTML 模板便能轻松定位在 JavaScript 代码里对应的方法。
  - 因为你无须在 JavaScript 里手动绑定事件，你的 ViewModel 代码可以是非常纯粹的逻辑，和 DOM 完全解耦，更易于测试。
  - 当一个 ViewModel 被销毁时，所有的事件处理器都会自动被删除。你无须担心如何清理它们。


# \# 表单输入绑定
- *修饰符*
  - .lazy
  ```html
  <!-- 在“change”时而非“input”时更新 
  即, 失去焦点时触发, 而不是数据更新时触发
  -->
  <input v-model.lazy="msg" >
  ```
  - .number
  如果想自动将用户的输入值转为数值类型，可以给 v-model 添加 number 修饰符,
  如果这个值无法被 parseFloat() 解析，则会返回原始的值。
  - .trim
  如果要自动过滤用户输入的首尾空白字符，可以给 v-model 添加 trim 修饰符 ;


# \# 组件
  - 一个组件的 data 选项必须是一个函数 , 并通过 return 返回相关数据
  - *父组件向子组件传递数据*: 在子组件中定义 prop 属性
  - *子组件通知父组件*: 在子组件中 `v-on:click="$emit('enlarge-text')"` 定义事件: enlarge-text, 使用此组件时, 在组件名上调用自定义事件: enlarge-text 即可: `<demo-button v-on:enlarge-text="hehe=2"></demo-button>`
  - *子组件传递参数到父组件*: 子向父通信可以通过 $event 访问到被抛出的这个值(参数)
  - 发现一个现象: template中需要 $emit 得用 `` 号, 用' 或者 " 都会报错, 无法成功
  - *动态和异步组件* : ...
  - *插槽* : ...
  - *获得组件的内置内置方法和属性* :
  ```js
  <template>
    <el-progress type="circle" :percentage="O" ref="progress"></el-progress>
  </template>
  <script>
    this.$refs.progress //组件对象实例， 可以手动调用组件的内置方法和属性
    this.$refs.progress.$el //组件 对象的最外层dom元素
  </script>
  ```


# \# 其他知识
- JSX 是使用 XML 语法编写 JavaScript 的一种语法糖。
  - 你可以使用完整的编程语言 JavaScript 功能来构建你的视图页面。比如你可以使用临时变量、JS 自带的流程控制、以及直接引用当前 JS 作用域中的值等等。
  - 开发工具对 JSX 的支持相比于现有可用的其他 Vue 模板还是比较先进的 (比如，linting、类型检查、编辑器的自动完成)。
- NativeScript 使用JavaScript创建本地iOS和Android应用程序




[1]: http://www.ruanyifeng.com/blog/2017/04/css_in_js.html